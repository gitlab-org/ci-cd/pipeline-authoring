<!-- 
Title should be:

Pipeline Authoring ##.# Planning issue
-->

### Pipeline Authoring Issue Boards

* [Pipeline Authoring workflow board](https://gitlab.com/gitlab-org/gitlab/-/boards/5726606?label_name[]=group%3A%3Apipeline%20authoring&milestone_title=Upcoming)
* [Pipeline Authoring needs weight board](https://gitlab.com/gitlab-org/gitlab/-/boards/5726606?label_name[]=group%3A%3Apipeline%20authoring&label_name[]=workflow%3A%3Aplanning%20breakdown)

### Goals for the milestone:

<!-- Replace these with the high-level goals for the milestone -->
* Goal number 1
* Goal number 2

### Current/Upcoming Security issues:

* [Current Security Issues Open](https://gitlab.com/groups/gitlab-org/-/issues/?sort=popularity&state=opened&label_name%5B%5D=devops%3A%3Averify&label_name%5B%5D=security&label_name%5B%5D=missed-SLO&label_name%5B%5D=group%3A%3Apipeline%20authoring&not%5Blabel_name%5D%5B%5D=type%3A%3Afeature&not%5Blabel_name%5D%5B%5D=documentation)
* [Upcoming Security Issues Due](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_asc&state=opened&label_name%5B%5D=devops%3A%3Averify&label_name%5B%5D=security&label_name%5B%5D=type%3A%3Abug&label_name%5B%5D=group%3A%3Apipeline%20authoring&not%5Blabel_name%5D%5B%5D=type%3A%3Afeature&not%5Blabel_name%5D%5B%5D=documentation&not%5Blabel_name%5D%5B%5D=severity%3A%3A4&not%5Blabel_name%5D%5B%5D=test-plan&not%5Blabel_name%5D%5B%5D=missed-SLO&first_page_size=20)

### Upcoming PTO in milestone
[Verify:Pipeline Authoring Google Calendar](https://calendar.google.com/calendar/u/0/embed?src=c_n7totcsnoi7l2j0a2n9ps08g7s@group.calendar.google.com&ctz=UTC)


###Milestone Deliverables

```glql
---
display: table
fields: title, state, healthStatus, epic, weight, labels("workflow::*"), assignee
limit: 15
---
label = ("group::pipeline authoring", "Deliverable") AND group = "gitlab-org"  AND milestone = "17.8"
```

### Scope of Work

<!-- Replace current version number -->

```glql
---
display: table
fields: title, state, healthStatus, epic, weight, labels("workflow::*"), assignee
limit: 40
---
label = ("group::pipeline authoring") AND group = "gitlab-org"  AND milestone = "17.8"
```

#### Active UX Research Efforts

<!-- Replace these with the UX Research Efforts for the milestone -->
* Effort number 1
* Effort number 2


/cc @gitlab-com/pipeline-authoring-group 

/label ~"Planning Issue"
