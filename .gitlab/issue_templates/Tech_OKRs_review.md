## Summary
This quarterly technical strategy review is intended to assess any changes that we feel are needed, whether that's swapping areas of focus or adding on ones that we didn't have before. Additionally, we will also look at the current OKRs for the quarter to ensure that we are aligned on them so that work that we do aligns to meaning.

## Goal
Under each thread below, we can discuss any open questions related to technical strategy or OKRs.  As noted above, this is a collaborative discussion so everyone shares their thoughts on our engineering direction.  We can make updates to these areas as needed from these discussions as well.

/cc @gitlab-com/pipeline-authoring-group/backend @gitlab-com/pipeline-authoring-group/frontend